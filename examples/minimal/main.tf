
module "operator" {
  source = "../../modules/operator"
}

resource "kubernetes_namespace" "acc_namespace" {
  metadata {
    name = "acc-tf-k8s-nginx-ingress"
  }
}


module "install" {
  depends_on = [module.operator]
  source     = "../../modules/install"
  namespace  = kubernetes_namespace.acc_namespace.metadata[0].name
}

output "helm_release" {
  value = module.install

}
