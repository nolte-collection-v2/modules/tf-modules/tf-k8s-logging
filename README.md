# TF Project tf-k8s-logging

```bash
export AWX_HOSTNAME="$(pass network/awx/host)" \
    && export AWX_USERNAME="$(pass network/awx/user)" \
    && export AWX_PASSWORD="$(pass network/awx/password)"
```
