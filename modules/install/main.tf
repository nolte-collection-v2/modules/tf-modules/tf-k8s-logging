
variable "name" {
  default = "logging"
}

resource "kubectl_manifest" "elastic" {
  yaml_body = <<YAML
apiVersion: elasticsearch.k8s.elastic.co/v1
kind: Elasticsearch
metadata:
  name: ${var.name}
  namespace: ${var.namespace}
spec:
  version: 7.9.2
  nodeSets:
  - name: default
    count: 1
    config:
      node.master: true
      node.data: true
      node.ingest: true
      node.store.allow_mmap: false
      # appender.rolling.layout.type: ESJsonLayout
      # appender.rolling.layout.type_name: server
YAML
}



resource "kubectl_manifest" "kibana" {
  depends_on = [kubectl_manifest.elastic]
  yaml_body  = <<YAML
apiVersion: kibana.k8s.elastic.co/v1
kind: Kibana
metadata:
  name: ${var.name}
  namespace: ${var.namespace}
spec:
  version: 7.9.2
  count: 1
  elasticsearchRef:
    name: ${var.name}
YAML
}

# ${var.name}-es-elastic-user
## ${var.name}-es-http-certs-internal
#    extraVolumeMounts = <<EOF
#    - name: cert-ca
#      mountPath: /etc/logstash/certificates
#      readOnly: true
#    EOF
locals {
  extra_values = {
    extraEnvs = [{
      name  = "ES_HOSTS"
      value = "https://${var.name}-es-http:9200"
      }, {
      name  = "ES_USER"
      value = "elastic"
      }, {
      name = "ES_PASSWORD"
      valueFrom = {
        secretKeyRef = {
          name = "${var.name}-es-elastic-user"
          key  = "elastic"
        }
      }
      }
    ]
  }
}

# https://github.com/elastic/helm-charts/blob/master/logstash/README.md
resource "helm_release" "release" {
  depends_on = [kubectl_manifest.elastic]
  name       = "logstash"
  repository = "https://Helm.elastic.co"
  chart      = "logstash"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    file("${path.module}/files/values.yml"),
    yamlencode(var.extra_values),
    yamlencode(local.extra_values),
  ]
}
