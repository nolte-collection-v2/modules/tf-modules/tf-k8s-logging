
resource "kubernetes_namespace" "acc_namespace" {
  metadata {
    name = "logging"
  }
}

module "operator" {
  source = "../operator"
}

module "install" {
  depends_on = [module.operator]
  source     = "../install"
  namespace  = kubernetes_namespace.acc_namespace.metadata[0].name
}
