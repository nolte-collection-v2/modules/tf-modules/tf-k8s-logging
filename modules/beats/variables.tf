variable "extra_values" {
  default = {}
}

variable "namespace" {
  type = string
}

variable "chart_version" {
    default = "7.9.2"
}
