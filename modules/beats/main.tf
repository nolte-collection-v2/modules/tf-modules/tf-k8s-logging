
variable "name" {
  default = "logging"
}

variable "beats_producer_name" {
  default = "myproject"
}

variable "beats_producer_id" {
  default = "574734885120952459"
}

variable "logstash_host" {
  default = "logstash-logstash:5044"
}

locals {
  extra_values = {
    extraEnvs = [{
      name  = "BEATS_PRODUCER_NAME"
      value = var.beats_producer_name
      },
      {
        name  = "BEATS_PRODUCER_ID"
        value = var.beats_producer_id
        }, {
        name  = "LOGSTASH_HOST"
        value = var.logstash_host
    }]
  }
}

# https://github.com/elastic/helm-charts/blob/master/filebeat/README.md
resource "helm_release" "release" {
  name       = "filebeat"
  repository = "https://Helm.elastic.co"
  chart      = "filebeat"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    file("${path.module}/files/values.yml"),
    yamlencode(var.extra_values),
    yamlencode(local.extra_values),
  ]
}
